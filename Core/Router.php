<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 25/10/2018
 * Time: 17:04
 */

class Router
{
    /**
     * Associative array of routes with their mapped [actions, controllers]. (The routing table)
     *
     * @var array
     */
    protected $routes = [];

    /**
     * Parameters from the matched route
     * @var array
     */
    protected $params = [];

    /**
     * Add a route to the routing table
     *
     * @param string $route The route URL
     * @param array $params Parameters (controller, action, etc.)
     *
     * @return void
     */

    public function add(string $route, array $params = []): void
    {
        $this->routes[$route] = $params;
        // Convert the  route to a regular expression : escape the forward slashes
        $route = preg_replace('~/~', '\\\\/', $route);
        // Convert variables e.g {controller}
        $route = preg_replace('~\{([a-z]+)\}~', '(?P<\1>[a-z-]+)', $route);
        // Add Start delimiter, end delimiters and case insensitive flag
        $route = '~^' . $route . '$~i';

        $this->routes[$route] = $params;
    }

    /**
     *  Get all the routes from the routing table
     *
     * @return array
     */
    public function getRoute(): array
    {
        return $this->routes;
    }

    /**
     * Get the currently matched parameters;
     *
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Match the route to the routes in the routing table, setting the params property if a route is found.
     *
     * @param string $url The route URL
     *
     * @return bool
     */
    public function match(string $url): bool
    {
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $params[$key] = $match;
                    }
                }
                $this->params = $params;
                return true;
            }
        }
        return false;
    }

    /**
     *
     *
     * @param string $url
     * @return  void
     */
    public function dispatch(string $url): void
    {
        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = $this->converToStudlyCaps($controller);

            if (class_exists($controller)) {
                $controller_object = new $controller();
                $action = $this->params['action'];
                $action = $this->converToCamelCase($action);

                if (is_callable([$controller_object, $action])) {
                    $controller_object->$action();
                } else {
                    echo "Method $action (in controller $controller) not found.";
                }
            } else {
                echo "Controller class $controller not found.";
            }
        } else {
            echo "No route matched.";
        }
    }

    /**
     * Convert the string with hyphens to StudlyCaps,
     * e.g post-authors => PostAuthors
     *
     * @param string $string The string to convert
     * @return string
     */
    public function converToStudlyCaps(string $string): string
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    /**
     * Convert the string with hyphens to CamelCase,
     * e.g add-new => addNew
     *
     * @param string $string The String to convert
     * @return string
     */
    public function converToCamelCase(string $string): string
    {
        return lcfirst($this->converToStudlyCaps($string));
    }
}