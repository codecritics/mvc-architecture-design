<?php
/**
 * Created by PhpStorm.
 * User: mariustanawatsamo
 * Date: 28/10/2018
 * Time: 14:53
 */

class Posts
{
    public function index()
    {
        echo 'Hello from the index action in the Posts controller!';
    }

    public function addNew()
    {
        echo 'Hello  from the addNew action in the Posts controller';
    }
}